<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
              block
            </h3>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">block</li>
              </ol>
            </nav>
          </div>
          <div class="row">
            <div class="col-8 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <form class="forms-sample">
                    <div class="form-group">
                      <label for="exampleSelectGender">Cluster</label>
                        <select class="form-control">
                          <option>Male</option>
                          <option>Female</option>
                        </select>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputName1">Unit</label>
                      <input type="text" class="form-control" id="exampleInputName1" placeholder="AA/99">
                    </div>                    
                    <p class="card-description">
                        Personal info
                    </p>
                    <div class="form-group">
                      <label for="exampleInputEmail3">No. ID</label>
                      <input type="email" class="form-control" placeholder="366-123456789">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail3">Full Name</label>
                      <input type="email" class="form-control" placeholder="Full name">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail3">Email address</label>
                      <input type="email" class="form-control" id="exampleInputEmail3" placeholder="Email">
                    </div>
                    <!-- <div class="form-group">
                      <label for="exampleInputPassword4">Password</label>
                      <input type="password" class="form-control" id="exampleInputPassword4" placeholder="Password">
                    </div> -->
                    <div class="form-group">
                      <label for="exampleSelectGender">Gender</label>
                        <select class="form-control" id="exampleSelectGender">
                          <option>Male</option>
                          <option>Female</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-gradient-primary mr-2">Submit</button>
                    <button class="btn btn-light">Cancel</button>
                  </form>
                </div>
              </div>
            </div>            
          </div>
        </div>
        <!-- content-wrapper ends -->
