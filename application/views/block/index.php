<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
              block
            </h3>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">block</li>
              </ol>
            </nav>
          </div>
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <p><a href="<?php echo site_url('block/add') ?>"><button type="button" class="btn btn-gradient-info btn-icon-text">
                    <i class="mdi mdi-plus btn-icon-prepend"></i> new claster
                  </button></a></p>
                  <table id="order-listing" class="table table-hover">
                    <thead>
                        <tr>
                            <th width="5%">ID</th>
                            <th>Cluster</th>
                            <th>No. Unit</th>
                            <th>Full Name</th>
                            <th>Cluster name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($blocks as $block) : ?>
                        <tr>
                            <td><?php echo $block->block_id ?></td>
                            <td><?php echo $block->cluster_name ?></td>
                            <td><?php echo $block->block_name ?></td>
                            <td><?php echo $block->full_name ?></td>
                            <td><?php echo $block->full_name ?></td>
                            <td>
                                <a href="" class="btn btn-sm"><i class="mdi mdi-table-edit"></i> Edit</a>
											          <a onclick="deleteConfirm('')" href="#!" class="btn btn-sm text-danger"><i class="mdi mdi-delete"></i> Hapus</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
