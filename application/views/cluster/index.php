<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
      <div class="main-panel">
        <div class="content-wrapper">
          <?php if ($this->session->flashdata('success')): ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
          <?php echo $this->session->flashdata('success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            </div>
          <?php endif; ?>
          <div class="page-header">
            <h3 class="page-title">
              Cluster
            </h3>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">cluster</li>
              </ol>
            </nav>
          </div>
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <!-- Button trigger modal -->
                  <p><button type="button" class="btn btn-gradient-info btn-icon-text" data-toggle="modal" data-target="#exampleModalCenter">
                    <i class="mdi mdi-plus btn-icon-prepend"></i>New cluster
                  </button></p>

                  <!-- Modal new cluster-->
                  <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLongTitle">New cluster</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                         <?php echo form_open('cluster/create') ?>
                            <div class="form-group row">
                              <label for="Cluster Name" class="col-sm-3 col-form-label">Cluster name</label>
                              <div class="col-sm-9">
                                <?php echo form_error('name') ?>
                                <input type="text" class="form-control" name="cluster_name" placeholder="new cluster" <?php echo form_error('cluster_name') ? 'is-invalid':'' ?> required>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                          <?php echo form_close() ?>
                        </div>
                      </div>
                    </div>
                  </div>

                  <table id="order-listing" class="table table-hover">
                    <thead>
                        <tr>
                            <th width="5%">ID</th>
                            <th>Cluster Name</th>
                            <th width="15%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($clusters as $cluster) : ?>
                        <tr>
                            <td><?php echo $cluster->cluster_id ?></td>
                            <td><?php echo $cluster->cluster_name ?></td>
                            <td>
                                <a href="" class="btn btn-sm" data-toggle="modal" data-target="#editModalCenter"><i class="mdi mdi-table-edit"></i> Edit</a>
											          <a onclick="deleteConfirm('')" href="#!" class="btn btn-sm text-danger"><i class="mdi mdi-delete"></i> Hapus</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal new cluster-->
        <div class="modal fade" id="editModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit cluster</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form class="form-sample">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Cluster name</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" name="cluster_name" placeholder="new cluster">
                    </div>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save</button>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
