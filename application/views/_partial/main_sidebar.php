<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid page-body-wrapper">
    <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
            <li class="nav-item">
            <a class="nav-link" href="index.html">
                <span class="menu-title">Dashboard</span>
                <i class="mdi mdi-home menu-icon"></i>
            </a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('cluster') ?>">
                <span class="menu-title">Cluster</span>
                <i class="mdi mdi-city menu-icon"></i>
            </a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('block') ?>">
                <span class="menu-title">Unit</span>
                <i class="mdi mdi-home-map-marker menu-icon"></i>
            </a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="pages/forms/basic_elements.html">
                <span class="menu-title">Transaction</span>
                <i class="mdi mdi-format-list-bulleted menu-icon"></i>
            </a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="pages/charts/chartjs.html">
                <span class="menu-title">Asset</span>
                <i class="mdi mdi-chart-bar menu-icon"></i>
            </a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="pages/tables/basic-table.html">
                <span class="menu-title">Report</span>
                <i class="mdi mdi-table-large menu-icon"></i>
            </a>
            </li>
        </ul>
    </nav>
