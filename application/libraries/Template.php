<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template
{
    protected $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
    }

    public function template_render($content, $data = NULL)
    {
        $this->template['header']       = $this->CI->load->view('_partial/header', $data, TRUE);
        $this->template['navbar']       = $this->CI->load->view('_partial/navbar', $data, TRUE);
        $this->template['content']      = $this->CI->load->view($content, $data, TRUE);
        $this->template['main_sidebar'] = $this->CI->load->view('_partial/main_sidebar', $data, TRUE);
        $this->template['footer']       = $this->CI->load->view('_partial/footer', $data, TRUE);
        
        return $this->CI->load->view('_partial/template', $this->template);
    }
    
}

/* End of file Template.php */
