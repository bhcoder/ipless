<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Block extends CI_Controller { 

public function __construct()
{
    parent::__construct();
    $this->load->model('block_model');
    $this->load->library('form_validation');
}

    public function index()
    {
        $data['blocks'] = $this->block_model->getAll();
        $this->template->template_render('block/index', $data);
    }

    public function create()
    {
        $this->template->template_render('block/create');
    }

}

/* End of file Block.php */
