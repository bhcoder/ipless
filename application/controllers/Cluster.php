<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Cluster extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('cluster_model');
        $this->load->library('form_validation');
        
    }
    
    public function index()
    {
        $data['clusters'] = $this->cluster_model->getAll();
        $this->template->template_render('cluster/index', $data);
    }

    public function create()
    {
        // $cluster    = $this->cluster_model;
        // $validation = $this->form_validation;
        // $validation->set_rules($cluster->rules());

        // if ($validation->run())
        // {
        //     $cluster->save();
        //     $this->session->set_flashdata('success', 'Save successfully');
        // }

        // $this->template->template_render('cluster/index');
        $data['cluster_name'] = $this->input->post("cluster_name");
        $this->cluster_model->save($data);

        $this->session->set_flashdata('success', 'Save Successfully');

        //redirect
        redirect('cluster');
    }

    public function edit($id)
    {
        $id = $this->uri->segment(3);
        $data['cluster_name'] = $this->cluster_model->edit($id);

        $this->session->set_flashdata('success', 'Save Successfully');
        // $this->template->template_render('cluster/index', $data);
        redirect('cluster');
    }
}

/* End of file Cluster.php */
