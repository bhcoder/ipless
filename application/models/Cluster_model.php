<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Cluster_model extends CI_Model {

    public function rules()
    {
        return [
            ['field' => 'cluster_name',
            'label'  => 'Cluster name',
            'rules'  => 'required']
        ];
    }

    public function getAll()
    {
        return $this->db->get('clusters')->result();
    }
    
    public function getById($id)
    {
        return $this->db->get_where('clusters', ['cluster_id' => $id])->row();
    }

    public function save($data)
    {
        // $post       = $this->input->post();
        // $this->cluster_name = $post['cluster_name'];
        // $this->db->insert('clusters', $this);
        $query = $this->db->insert("clusters", $data);

        if($query){
            return true;
        }else{
            return false;
        }
    }

    public function update()
    {
        $post               = $this->input->post();
        $this->cluster_id   = $post['cluster_id'];
        $this->name         = $post['name'];
        $this->db->update('clusters', $this, array('cluster_id' => $post['cluster_id']));
    }

    public function delete($id)
    {
        return $this->db->delate('clusters', array('cluster_id' => $id));
    }
}

/* End of file Cluster_model.php */
