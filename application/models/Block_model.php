<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Block_model extends CI_Model {

    public function rules()
    {

    }

    public function getAllclusters()
    {
        return $this->db->get('clusters')->result();
    }

    public function getAll()
    {
        $this->db->select('*');
        $this->db->from('blocks');
        $this->db->join('clusters','clusters.cluster_id = blocks.block_id');

        return $this->db->get()->result();
    }

    public function getById($id)
    {
        return $this->db->get_where('blocks', ['block_id' => $id])->row();
    }

    public function save()
    {
        $post       = $this->input->post();
        $this->name = $post['name'];
        $this->db->insert('blocks', $this);
    }

    public function update()
    {
        $post               = $this->input->post();
        $this->block_id   = $post['block_id'];
        $this->name         = $post['name'];
        $this->db->update('blocks', $this, array('block_id' => $post['block_id']));
    }

    public function delete($id)
    {
        return $this->db->delate('blocks', array('block_id' => $id));
    }

}

/* End of file Block_model.php */
